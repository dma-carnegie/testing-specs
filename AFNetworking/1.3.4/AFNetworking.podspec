Pod::Spec.new do |s|
  s.name     = 'AFNetworking'
  s.version  = '1.3.4'
  s.license  = 'MIT'
  s.summary  = 'A delightful iOS and OS X networking framework.'
  s.homepage = 'https://gitlab.pravala.com/cocoa-pods/afnetworking'
  s.authors  = { 'Mattt Thompson' => 'm@mattt.me', 'Scott Raymond' => 'sco@gowalla.com' }
  s.source   = { :git => 'https://gitlab.pravala.com/cocoa-pods/afnetworking', :tag => '1.3.4' }
  s.source_files = 'AFNetworking/AFNetworking'
  s.requires_arc = true
  s.prepare_command = <<-CMD
    git submodule update --init --recursive
    cd AFNetworking
    patch -Np1 -i ../custom_url_connection_class.patch
  CMD

  s.ios.deployment_target = '5.0'
  s.ios.frameworks = 'MobileCoreServices', 'SystemConfiguration', 'Security', 'CoreGraphics'

  s.osx.deployment_target = '10.7'
  s.osx.frameworks = 'CoreServices', 'SystemConfiguration', 'Security'

  s.prefix_header_contents = <<-EOS
#import <Availability.h>

#if __IPHONE_OS_VERSION_MIN_REQUIRED
  #import <SystemConfiguration/SystemConfiguration.h>
  #import <MobileCoreServices/MobileCoreServices.h>
  #import <Security/Security.h>
#else
  #import <SystemConfiguration/SystemConfiguration.h>
  #import <CoreServices/CoreServices.h>
  #import <Security/Security.h>
#endif
EOS
end
