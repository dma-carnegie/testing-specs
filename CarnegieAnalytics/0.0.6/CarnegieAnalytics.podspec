Pod::Spec.new do |spec|
  spec.name     = 'CarnegieAnalytics'
  spec.version  = '0.0.6'
  spec.author   = 'Carnegie Technologies'
  spec.license  = 'Commercial'
  spec.homepage = 'https://www.carnegietechnologies.com/'
  spec.source   = { :git => 'https://g.ctech.rocks/cocoa-pods/analytics.git',
                    :tag => "#{spec.version}" }
  spec.summary  = 'Analytics'
  spec.platform = :ios, '7.0'

  spec.source_files        = 'analytics/*.h'
  spec.public_header_files = 'analytics/*.h'
  spec.header_dir          = 'Analytics'
  spec.vendored_libraries  = 'analytics/libIosAnalytics.a'
  spec.preserve_paths      = 'analytics/*.a'

  spec.dependency 'CarnegieVTunSocket', '>= 0.0.16'
end
