Pod::Spec.new do |spec|
  spec.name     = 'CarnegieMasSDK'
  spec.version  = '1.0.57'
  spec.author   = 'Carnegie Technologies'
  spec.license  = 'Commercial'
  spec.homepage = 'https://www.carnegietechnologies.com/'
  spec.source   = { :git => 'https://g.ctech.rocks/cocoa-pods/mas-sdk.git',
                    :tag => "#{spec.version}" }
  spec.summary  = 'MAS SDK for iOS'
  spec.platform = :ios, '7.0'

  spec.source_files        = 'mas-sdk/include/MasSDK/*.h'
  spec.public_header_files = 'mas-sdk/include/MasSDK/*.h'
  spec.header_dir          = 'MasSDK'
  spec.vendored_libraries  = 'mas-sdk/lib/libIosMasSDK.a', 'mas-sdk/lib/libJWT.a', 'mas-sdk/lib/libMasSDK.a'
  spec.preserve_paths      = 'mas-sdk/lib/*.a'
  spec.libraries           = 'c++', 'z', 'resolv', 'IosMasSDK', 'JWT', 'MasSDK'

  spec.dependency 'OpenSSL-Universal', '1.0.1.20'
  spec.dependency 'CarnegieVTunSocket', '>= 0.0.16'
  spec.dependency 'CarnegieAnalytics', '>= 0.0.6'
end
