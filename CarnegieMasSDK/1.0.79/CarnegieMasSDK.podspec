Pod::Spec.new do |spec|
  spec.name     = 'CarnegieMasSDK'
  spec.version  = '1.0.79'
  spec.author   = 'Carnegie Technologies'
  spec.license  = 'Commercial'
  spec.homepage = 'https://www.carnegietechnologies.com/'
  spec.source   = { :git => 'https://gitlab.com/dma-carnegie/mas-sdk.git',
                    :tag => "#{spec.version}" }
  spec.summary  = 'MAS SDK for iOS'
  spec.platform = :ios, '7.0'

  spec.source_files        = 'MasSDK/include/*.h'
  spec.public_header_files = 'MasSDK/include/*.h'
  spec.header_dir          = 'MasSDK'
  spec.vendored_libraries  = 'MasSDK/lib/libIosMasSDK.a', 'MasSDK/lib/libJWT.a', 'MasSDK/lib/libMasSDK.a'
  spec.preserve_paths      = 'MasSDK/lib/*.a'
  spec.libraries           = 'c++', 'z', 'resolv', 'IosMasSDK', 'JWT', 'MasSDK'

  spec.ios.framework = 'NetworkExtension'

  spec.dependency 'OpenSSL-Universal', '1.0.1.20'
  spec.dependency 'CarnegiePravCore', '>= 0.0.3'
  spec.dependency 'CarnegieVTunSocket', '>= 0.0.18'
  spec.dependency 'CarnegieNcpWebClient', '>= 0.0.4'
end
