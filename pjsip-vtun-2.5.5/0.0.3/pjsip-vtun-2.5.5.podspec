Pod::Spec.new do |s|
  s.name         = 'pjsip-vtun-2.5.5'
  s.version      = '0.0.3'
  s.summary      = 'Open Source SIP, Media and NAT Traversal Library'
  s.homepage     = 'http://www.pjsip.org'
  s.author       = 'www.pjsip.org'
  s.source       = { :git => 'https://gitlab.pravala.com/cocoa-pods/pjsip-vtun.git', :tag => "#{s.version}" }
  s.platform     = :ios, '7.0'
  s.description  = <<-DESC
PJSIP is a free and open source multimedia communication library written in C language implementing standard
based protocols such as SIP, SDP, RTP, STUN, TURN, and ICE. It combines signaling protocol (SIP) with rich
multimedia framework and NAT traversal functionality into high level API that is portable and suitable for
almost any type of systems ranging from desktops, embedded systems, to mobile handsets.

PJSIP is both compact and feature rich. It supports audio, video, presence, and instant messaging, and has
extensive documentation. PJSIP is very portable. On mobile devices, it abstracts system dependent features
and in many cases is able to utilize the native multimedia capabilities of the device.

PJSIP has been developed by a small team working exclusively for the project since 2005, with participation
of hundreds of developers from around the world, and is routinely tested at SIP Interoperability Event
(SIPit) since 2007.

This version of PJSIP 2.5.5 has been modified to use Pravala's virtual tunnel sockets, and is built with
support for the included Opus codec.
                    DESC
  s.license      = {
     :type => 'Dual-License',
     :text => <<-LICENSE
PJSIP source code ("The Software") is licensed under both General Public License (GPL) version 2 or later
and a proprietary license that can be arranged with us. In practical sense, this means:
if you are developing Open Source Software (OSS) based on PJSIP, chances are you will be able to use PJSIP
freely under GPL. But please double check here for OSS license compatibility with GPL.

Alternatively, if you are unable to release your application as Open Source Software, you may arrange
alternative licensing with us. Just send your inquiry to licensing@teluu.com to discuss this option.

PJSIP may include third party software in its source code distribution. Third Party Software does not
comprise part of "The Software". Please make sure that you comply with the licensing term of each software.
               LICENSE
   }

  s.source_files        =
  s.public_header_files =['pjsip-vtun-2.5.5/pjlib/include/*.h',
                          'pjsip-vtun-2.5.5/pjlib/include/**/*.h',
                          'pjsip-vtun-2.5.5/pjlib-util/include/**/*.h',
                          'pjsip-vtun-2.5.5/pjlib-util/include/*.h',
                          'pjsip-vtun-2.5.5/pjmedia/include/**/*.h',
                          'pjsip-vtun-2.5.5/pjmedia/include/*.h',
                          'pjsip-vtun-2.5.5/pjnath/include/**/*.h',
                          'pjsip-vtun-2.5.5/pjnath/include/*.h',
                          'pjsip-vtun-2.5.5/pjsip/include/**/*.h',
                          'pjsip-vtun-2.5.5/pjsip/include/*.h']

  s.preserve_paths      =['pjsip-vtun-2.5.5/pjlib/include/**/*',
                          'pjsip-vtun-2.5.5/pjlib/include/*',
                          'pjsip-vtun-2.5.5/pjlib-util/include/**/*',
                          'pjsip-vtun-2.5.5/pjlib-util/include/*',
                          'pjsip-vtun-2.5.5/pjmedia/include/**/*',
                          'pjsip-vtun-2.5.5/pjmedia/include/*',
                          'pjsip-vtun-2.5.5/pjnath/include/**/*',
                          'pjsip-vtun-2.5.5/pjnath/include/*',
                          'pjsip-vtun-2.5.5/pjsip/include/**/*',
                          'pjsip-vtun-2.5.5/pjsip/include/*']

  s.vendored_libraries  =['pjsip-vtun-2.5.5/pjlib/lib/*.a',
                          'pjsip-vtun-2.5.5/pjlib-util/lib/*.a',
                          'pjsip-vtun-2.5.5/pjmedia/lib/*.a',
                          'pjsip-vtun-2.5.5/pjnath/lib/*.a',
                          'pjsip-vtun-2.5.5/pjsip/lib/*.a',
                          'pjsip-vtun-2.5.5/third_party/lib/*.a',
                          'pjsip-vtun-2.5.5/codecs/lib/*.a']

  header_search_paths   =["$(PODS_ROOT)/pjsip-vtun-2.5.5/pjsip-vtun-2.5.5/pjlib/include",
                          "$(PODS_ROOT)/pjsip-vtun-2.5.5/pjsip-vtun-2.5.5/pjlib-util/include",
                          "$(PODS_ROOT)/pjsip-vtun-2.5.5/pjsip-vtun-2.5.5/pjmedia/include",
                          "$(PODS_ROOT)/pjsip-vtun-2.5.5/pjsip-vtun-2.5.5/pjnath/include",
                          "$(PODS_ROOT)/pjsip-vtun-2.5.5/pjsip-vtun-2.5.5/pjsip/include"]

  s.xcconfig            = {
      'HEADER_SEARCH_PATHS'          => header_search_paths.join(' '),
      'GCC_PREPROCESSOR_DEFINITIONS' => 'PJ_AUTOCONF=1'
  }

  s.dependency            'OpenSSL-Universal', '1.0.1.20'
  s.dependency            'VTunSocket', '0.0.5'
  s.header_mappings_dir = ''
  s.requires_arc        = false
end

