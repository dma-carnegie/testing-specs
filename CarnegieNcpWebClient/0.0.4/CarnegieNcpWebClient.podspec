Pod::Spec.new do |spec|
  spec.name     = 'CarnegieNcpWebClient'
  spec.version  = '0.0.4'
  spec.author   = 'Carnegie Technologies'
  spec.license  = 'Commercial'
  spec.homepage = 'https://www.carnegietechnologies.com/'
  spec.source   = { :git => 'https://gitlab.com/dma-carnegie/ncpwebclient.git',
                    :tag => "#{spec.version}" }
  spec.summary  = 'NCP web client module'
  spec.platform = :ios, '7.0'

  spec.source_files        = 'NcpWebClient/include/*.h'
  spec.public_header_files = 'NcpWebClient/include/*.h'
  spec.header_dir          = 'NcpWebClient'
  spec.vendored_libraries  = 'NcpWebClient/lib/libIosNcpWebClient.a'
  spec.preserve_paths      = 'NcpWebClient/lib/*.a'

  spec.dependency 'CarnegiePravCore', '>= 0.0.3'
  spec.dependency 'CarnegieSimpleLog', '>= 0.0.1'
end
