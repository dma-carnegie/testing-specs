Pod::Spec.new do |spec|
  spec.name     = 'CarnegieVTunSocket'
  spec.version  = '0.0.18'
  spec.author   = 'Carnegie Technologies'
  spec.license  = 'Commercial'
  spec.homepage = 'https://www.carnegietechnologies.com/'
  spec.source   = { :git => 'https://gitlab.com/dma-carnegie/vtunsocket.git',
                    :tag => "#{spec.version}" }
  spec.summary  = 'Virtual tunnel sockets'
  spec.platform = :ios, '7.0'

  spec.source_files        = 'vtunsocket/include/*.h'
  spec.public_header_files = 'vtunsocket/include/*.h'
  spec.header_dir          = 'VTunSocket'
  spec.vendored_libraries  = 'vtunsocket/lib/libIosVTunSocket.a', 'vtunsocket/lib/libVTunSocket.a'
  spec.preserve_paths      = 'vtunsocket/lib/*.a'

  spec.dependency 'CarnegieDns', '>= 0.0.1'
  spec.dependency 'CarnegieSimpleLog', '>= 0.0.1'
end
