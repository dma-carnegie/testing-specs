Pod::Spec.new do |spec|
  spec.name     = 'CarnegiePravCore'
  spec.version  = '0.0.4'
  spec.author   = 'Carnegie Technologies'
  spec.license  = 'Commercial'
  spec.homepage = 'https://www.carnegietechnologies.com/'
  spec.source   = { :git => 'https://gitlab.com/dma-carnegie/pravcore.git',
                    :tag => "#{spec.version}" }
  spec.summary  = 'Pravala core module'
  spec.platform = :ios, '7.0'

  spec.source_files        = 'PravCore/include/*.h'
  spec.public_header_files = 'PravCore/include/*.h'
  spec.header_dir          = 'PravCore'
  spec.vendored_libraries  = 'PravCore/lib/libIosPravCore.a'
  spec.preserve_paths      = 'PravCore/lib/*.a'
end
