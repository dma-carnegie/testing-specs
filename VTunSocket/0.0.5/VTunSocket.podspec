Pod::Spec.new do |spec|
  spec.name     = 'VTunSocket'
  spec.version  = '0.0.5'
  spec.author   = 'Pravala Networks'
  spec.license  = 'Commercial'
  spec.homepage = 'http://pravala.com'
  spec.source   = { :git => 'https://gitlab.pravala.com/cocoa-pods/vtunsocket.git',
                    :tag => "#{spec.version}" }
  spec.summary  = 'Virtual tunnel sockets'

  spec.ios.deployment_target   = '6.0'
  spec.ios.source_files        = 'vtunsocket/*.h'
  spec.ios.public_header_files = 'vtunsocket/*.h'
  spec.ios.header_dir          = 'VTunSocket'
  spec.ios.vendored_libraries  = 'vtunsocket/libIosVTunSocket.a', 'vtunsocket/libVTunSocket.a'
  spec.ios.preserve_paths      = 'vtunsocket/*.a'
end
