Pod::Spec.new do |spec|
  spec.name     = 'VTunSocket'
  spec.version  = '0.0.12'
  spec.author   = 'Pravala Networks'
  spec.license  = 'Commercial'
  spec.homepage = 'http://pravala.com'
  spec.source   = { :git => 'https://g.ctech.rocks/cocoa-pods/vtunsocket.git',
                    :tag => "#{spec.version}" }
  spec.summary  = 'Virtual tunnel sockets'
  spec.platform = :ios, '7.0'

  spec.source_files        = 'vtunsocket/*.h'
  spec.public_header_files = 'vtunsocket/*.h'
  spec.header_dir          = 'VTunSocket'
  spec.vendored_libraries  = 'vtunsocket/libIosVTunSocket.a', 'vtunsocket/libVTunSocket.a'
  spec.preserve_paths      = 'vtunsocket/*.a'
end
