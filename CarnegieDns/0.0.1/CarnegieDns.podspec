Pod::Spec.new do |spec|
  spec.name     = 'CarnegieDns'
  spec.version  = '0.0.1'
  spec.author   = 'Carnegie Technologies'
  spec.license  = 'Commercial'
  spec.homepage = 'https://www.carnegietechnologies.com/'
  spec.source   = { :git => 'https://gitlab.com/dma-carnegie/dns.git',
                    :tag => "#{spec.version}" }
  spec.summary  = 'DNS resolver module'
  spec.platform = :ios, '7.0'

  spec.source_files        = 'dns/include/*.h'
  spec.public_header_files = 'dns/include/*.h'
  spec.header_dir          = 'Dns'
  spec.vendored_libraries  = 'dns/lib/libIosDns.a'
  spec.preserve_paths      = 'dns/lib/*.a'

  spec.dependency 'CarnegieSimpleLog', '>= 0.0.1'
end
