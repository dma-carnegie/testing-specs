Pod::Spec.new do |spec|
  spec.name     = 'MasSDK'
  spec.version  = '0.0.1'
  spec.author   = 'Pravala Networks'
  spec.license  = 'Commercial'
  spec.homepage = 'http://pravala.com'
  spec.source   = { :git => 'https://gitlab.pravala.com/cocoa-pods/mas-sdk.git',
                    :tag => "#{spec.version}" }
  spec.summary  = 'ACE+ SDK for iOS'

  spec.ios.deployment_target   = '6.0'
  spec.ios.source_files        = 'mas-sdk/include/MasSDK/*.h'
  spec.ios.public_header_files = 'mas-sdk/include/MasSDK/*.h'
  spec.ios.header_dir          = 'MasSDK'
  spec.ios.vendored_libraries  = 'mas-sdk/lib/libIosMasSDK.a', 'mas-sdk/lib/libMasSDK.a'
  spec.ios.preserve_paths      = 'mas-sdk/lib/*.a'
  spec.ios.libraries           = 'c++', 'z', 'resolv', 'IosMasSDK', 'MasSDK'
  spec.ios.dependency 'OpenSSL-Universal', '1.0.1.20'
  spec.ios.dependency 'VTunSocket'
end
