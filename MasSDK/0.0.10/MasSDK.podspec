Pod::Spec.new do |spec|
  spec.name     = 'MasSDK'
  spec.version  = '0.0.10'
  spec.author   = 'Pravala Networks'
  spec.license  = 'Commercial'
  spec.homepage = 'http://pravala.com'
  spec.source   = { :git => 'https://gitlab.pravala.com/cocoa-pods/mas-sdk.git',
                    :tag => "#{spec.version}" }
  spec.summary  = 'ACE+ SDK for iOS'
  spec.platform = :ios, '7.0'

  spec.source_files        = 'mas-sdk/include/MasSDK/*.h'
  spec.public_header_files = 'mas-sdk/include/MasSDK/*.h'
  spec.header_dir          = 'MasSDK'
  spec.vendored_libraries  = 'mas-sdk/lib/libIosMasSDK.a', 'mas-sdk/lib/libJWT.a', 'mas-sdk/lib/libMasSDK.a'
  spec.preserve_paths      = 'mas-sdk/lib/*.a'
  spec.libraries           = 'c++', 'z', 'resolv', 'IosMasSDK', 'JWT', 'MasSDK'

  spec.dependency 'OpenSSL-Universal', '1.0.1.20'
  spec.dependency 'VTunSocket'
end
